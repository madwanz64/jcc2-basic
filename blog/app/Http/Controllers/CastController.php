<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index(){
        $casts = DB::table('cast')->get();
        return view('cast.index', compact('casts'));
    }
    public function create(){
        return view('cast.create');
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required|max:255',
            'umur' => 'required|integer',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')->insert([ 
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
            ]);
            $cast_name = $request['nama'];
            return redirect('/cast')->with('success',"Cast $cast_name berhasil ditambahkan");
    }
    public function show($cast_id){
        $cast = DB::table('cast')->where('id', $cast_id)->first();
        return view('cast.show', compact('cast'));
    }
    public function edit($cast_id){
        $cast = DB::table('cast')->where('id', $cast_id)->first();
        return view('cast.edit', compact('cast'));
    }
    public function update($cast_id, Request $request){
        $request->validate([
            'nama' => 'required|max:255',
            'umur' => 'required|integer',
            'bio' => 'required',
        ]);
        $query = DB::table('cast')
        ->where('id', $cast_id)
        ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]);
            $cast_name = $request['nama'];
            return redirect('/cast')->with('success', "Berhasil mengupdate data cast $cast_name");
    }
    public function destroy($cast_id){
        DB::table('cast')->where('id', $cast_id)->delete();
        return redirect('/cast')->with('success', "Cast sudah berhasil dihapus!");
    }
}
