@extends('adminLTE.master')

@section('judul')
    Daftar Cast
@endsection
@section('action')
    <a href="/cast/create" class="btn btn-sm btn-info">Tambah</a>
@endsection
@section('sessionAlert')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
@endsection

@section('content')
    <div class="row">
        @forelse ($casts as $key => $cast)
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ $key + 1 }}. {{ $cast->nama }}</h3>
                        <div class="card-tools">
                            <span class="badge badge-info">{{ $cast->umur }} tahun</span>
                        </div>
                    </div>
                    <div class="card-body">
                        {{ Str::limit($cast->bio, 60) }}
                    </div>
                    <div class="card-footer ">
                        <div class="row d-flex justify-content-end">
                            <a href="/cast/{{ $cast->id }}" class="btn btn-sm btn-info mx-2">Detail</a>
                            <a href="/cast/{{ $cast->id }}/edit" class="btn btn-sm btn-outline-info mx-2">Edit</a>
                            <form action="/cast/{{ $cast->id }}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="Delete" onclick="return confirm('Delete cast?')"
                                    class="btn btn-sm btn-danger mx-2">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            Belum ada data cast di dalam database
        @endforelse
    </div>
@endsection
